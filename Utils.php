<?php 

declare(strict_types=1);

namespace Komalbarun;


class Utils
{
	public static function pre_view($data)
	{
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	} 

	public static function view_gen($gen)
	{
		foreach ($gen as $value) 
		{
			self::pre_view($value);
		}
	}

	public static function custom_session_timeout(string $seconds, 
												  string $probability,
												  string $divisor,
												  string $path)
	{
		ini_set('session.gc_maxlifetime', $seconds);
		ini_set('session.gc_probability', $probability);
		ini_set('session.gc_divisor', $divisor);
		session_save_path($path);
	}

	public static function make_session()
	{
		if(session_status()==PHP_SESSION_NONE)
		{
			session_start();
		}
	}

	public static function redirect(string $url)
	{
		header("Location:{$url}"); die();
	}

}