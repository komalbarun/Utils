## Utils
--------
 Basic PHP utilities

## Installation
----------------
 composer require komalbarun\utils

## Dependencies
----------------
 * php >= 7.0

## Usage
---------
```php
<?php

require_once './vendor/autoload.php';

use Komalbarun\Utils;

// print_r($data) in a html <pre> tag;
Utils::pre_view($data);

// 'loops' over a generator object and print_r($object) for each object.
Utils::view_gen($generator_object);

// Creates a session if not already created
Utils:make_session();

Utils::redirect($url);

```
